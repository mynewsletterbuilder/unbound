#!/bin/bash

if [ $UNBOUND_THREADS ]; then
    echo "    num-threads: ${UNBOUND_THREADS}" >> /etc/unbound/unbound.conf
else
    echo "    num-threads: 1" >> /etc/unbound/unbound.conf
fi

if [ $UNBOUND_IFACE ]; then
    echo "    interface: ${UNBOUND_IFACE}" >> /etc/unbound/unbound.conf
else
    echo "    interface: 0.0.0.0" >> /etc/unbound/unbound.conf
fi

exec "$@"